package com.tcbakes.crud.impl.base;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FakeManagedCrud.class)
public abstract class FakeManagedCrud_ {

	public static volatile SingularAttribute<FakeManagedCrud, Long> id;
	public static volatile SingularAttribute<FakeManagedCrud, String> name;

}

