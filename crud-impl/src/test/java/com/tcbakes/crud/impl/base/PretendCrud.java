package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.tools.ProvidesBackingForDto;


@ProvidesBackingForDto(PretendCrudDto.class)
public class PretendCrud extends AbstractPretendCrud {

	private static final long serialVersionUID = 1L;
	
	String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void initializeFrom(AbstractPretendCrudDto dto) {
		from(dto);
		this.description = ((PretendCrudDto)dto).getDescription();
	}

	@Override
	public AbstractPretendCrudDto toDto() {
		PretendCrudDto d = new PretendCrudDto();
		to(d);
		d.setDescription(this.description);
		
		return d;
	}

}
