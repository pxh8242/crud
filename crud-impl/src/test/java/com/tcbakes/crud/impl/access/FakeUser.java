package com.tcbakes.crud.impl.access;

import com.tcbakes.crud.spec.access.UserDto;

public class FakeUser extends User {

	private static final long serialVersionUID = 1L;

	@Override
	public void initializeFrom(UserDto dto) {
		initialize(dto);
	}

	@Override
	public UserDto toDto() {
		FakeUserDto dto = new FakeUserDto();
		super.to(dto);
		
		return dto;
	}

    @Override
    public String getFriendlyUsername() {
        return "FakeUser";
    }
}
