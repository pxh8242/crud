package com.tcbakes.crud.impl.base;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

public class CrudServiceTest {

	@InjectMocks
	PseudoCrudService service;
	@Mock
	PseudoCrud mockFake;
	@Mock
	PageOfItems<PseudoCrud, PseudoCrudDto, Long> mockPage;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		when(mockFake.toDto()).thenReturn(new PseudoCrudDto());
		when(mockPage.toDto()).thenReturn(new PageOfItemsDto<PseudoCrudDto>());
	}

	@Test
	public void getByIdTest() {
		when(
				service.getItemFactory().findAndBuild(new Long(123),
						PseudoCrud.class, null)).thenReturn(mockFake);

		PseudoCrudDto u = service.read(new Long(123));
		Assert.assertNotNull(u);
	}

	@Test(expected = NotFoundException.class)
	public void getByIdTest_NotFound() {

		when(
				service.getItemFactory().findAndBuild(new Long(123),
						PseudoCrud.class, null)).thenReturn(null);

		service.read(new Long(123));
	}

	@Test(expected = BadRequestException.class)
	public void getByIdTest_NullId() {

		service.read(null);
	}

	@Test(expected = ServiceException.class)
	public void getByIdTest_Error() {
		Long id = new Long(123);
		when(service.getItemFactory().findAndBuild(id, PseudoCrud.class, null))
				.thenThrow(new RuntimeException());

		service.read(id);
	}
	
	@Test
	public <C, I> void createTest() {

		PseudoCrudDto fakeCrudDto = new PseudoCrudDto();

		when(service.getItemFactory().build(fakeCrudDto, null))
				.thenReturn(mockFake);

		PseudoCrudDto u = service.create(fakeCrudDto);

		Assert.assertNotNull(u);
	}
	
	@Test(expected = ValidationException.class)
	public void createTest_InvalidData() {
		when(
				service.getItemFactory().build(any(PseudoCrudDto.class),
						any(ItemPreparer.class))).thenThrow(
				new CrudItemValidationException());

		service.create(new PseudoCrudDto());
	}

	@Test(expected = BadRequestException.class)
	public void createTest_IllegalArgument() {
		service.create(null);
	}
	
	@Test(expected = BadRequestException.class)
	public void createTest_ReferencedEntityNotFound() {

		when(
				service.getItemFactory().build(any(PseudoCrudDto.class),
						any(ItemPreparer.class))).thenThrow(
				new BadRequestException());

		service.create(new PseudoCrudDto());
	}

	@Test(expected = ServiceException.class)
	public void createTest_Error() {
		when(service.getItemFactory().build(any(PseudoCrudDto.class)))
				.thenThrow(new RuntimeException());
		service.create(new PseudoCrudDto());
	}
	
	@Test
	public void updateTest() {
		Long id = new Long(0);
		PseudoCrudDto dto = new PseudoCrudDto();

		PseudoCrud existingC = mock(PseudoCrud.class);

		PseudoCrud newC = mock(PseudoCrud.class);

		when(newC.toDto()).thenReturn(new PseudoCrudDto());
		when(service.getItemFactory().build(dto, null)).thenReturn(newC);
		when(service.getItemFactory().findAndBuild(id, PseudoCrud.class, null))
				.thenReturn(existingC);

		PseudoCrudDto u = service.update(id, dto);

		Assert.assertNotNull(u);
	}
	
	@Test(expected = NotFoundException.class)
	public void updateTest_NotFound() {
		Long id = new Long(0);
		when(service.getItemFactory().findAndBuild(id, PseudoCrud.class, null))
				.thenReturn(null);
		service.update(id, new PseudoCrudDto());
	}
	
	@Test(expected = ValidationException.class)
	public void updateTest_InvalidData() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						PseudoCrud.class, null)).thenThrow(
				new CrudItemValidationException());
		service.update(new Long(0), new PseudoCrudDto());
	}
	
	@Test(expected = BadRequestException.class)
	public void updateTest_IllegalArg1() {

		service.update(null, new PseudoCrudDto());
	}

	@Test(expected = BadRequestException.class)
	public void updateTest_IllegalArg2() {

		service.update(new Long(123), null);
	}
	
	@Test(expected = ServiceException.class)
	public void updateTest_Error() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						PseudoCrud.class, null)).thenThrow(
				new RuntimeException());
		service.update(new Long(0), new PseudoCrudDto());
	}

	@Test
	public void deleteTest() {
		Long id = new Long(0);
		when(service.getItemFactory().findAndBuild(id, PseudoCrud.class, null))
				.thenReturn(mockFake);
		service.delete(id);

	}
	
	@Test(expected = NotFoundException.class)
	public void deleteTest_NotFound() {
		Long id = new Long(0);
		when(service.getItemFactory().findAndBuild(id, PseudoCrud.class, null))
				.thenReturn(null);
		service.delete(id);

	}

	@Test(expected = BadRequestException.class)
	public void deleteTest_BadRequest() {

		service.delete(null);

	}
	
	@Test(expected = ServiceException.class)
	public void deleteTest_Error() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						PseudoCrud.class, null)).thenThrow(
				new RuntimeException());
		service.delete(new Long(0));

	}

	@Test
	public void getPageTest() {

		when(service.getItemFactory().buildPage(PseudoCrud.class, null))
				.thenReturn(mockPage);

		PageOfItemsDto<PseudoCrudDto> d = service.getPage(1, 10);

		Assert.assertNotNull(d);
	}
}
