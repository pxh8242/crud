package com.tcbakes.crud.impl.base;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.impl.access.ACL;
import com.tcbakes.crud.impl.access.AccessToken;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.access.ACLDto;
import com.tcbakes.crud.spec.access.OperationListDto;
import com.tcbakes.crud.spec.exception.AccessUnauthorizedException;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

public class ManagedCrudServiceTest {

	@InjectMocks
	FakeManagedCrudService service;

	@Mock
	FakeManagedCrud mockFake;
	@Mock
	AccessToken mockAccessToken;
	@Mock
	PageOfItems<FakeManagedCrud, FakeManagedCrudDto, Long> mockPage;
	@Mock
	ItemPreparer<FakeManagedCrud, FakeManagedCrudDto, Long> mockPreparer;
	@Mock
	ACL mockACL;
	
	String mockUuid = "7f7c09b5-68a8-4562-8395-7dc3c9e7538e";

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		when(mockFake.getAcl()).thenReturn(mockACL);

		when(mockACL.toDto()).thenReturn(new ACLDto());

		when(mockFake.toDto()).thenReturn(new FakeManagedCrudDto());

		when(mockAccessToken.isValid()).thenReturn(true);

		// Set up the locator so that it always finds the AccessToken
		when(
				service.getAccessFactory().findAndBuild(mockUuid,
						AccessToken.class, null)).thenReturn(mockAccessToken);
		
		when(mockPage.toDto()).thenReturn(
				new PageOfItemsDto<FakeManagedCrudDto>());

	}

	@Test
	public void getByIdTest() {
		when(
				service.getItemFactory().findAndBuild(new Long(123),
						FakeManagedCrud.class, null)).thenReturn(mockFake);
		when(mockFake.toDto()).thenReturn(new FakeManagedCrudDto());

		FakeManagedCrudDto u = service.read(new Long(123), mockUuid);
		Assert.assertNotNull(u);
	}

	@Test(expected = NotFoundException.class)
	public void getByIdTest_NotFound() {

		when(
				service.getItemFactory().findAndBuild(new Long(123),
						FakeManagedCrud.class, null)).thenReturn(null);

		service.read(new Long(123), mockUuid);
	}

	@Test(expected = BadRequestException.class)
	public void getByIdTest_NullId() {
		doThrow(new BadRequestException()).when(mockFake).read();

		service.read(null, mockUuid);
	}

	@Test(expected = ServiceException.class)
	public void getByIdTest_Error() {
		Long id = new Long(123);
		when(service.getItemFactory().findAndBuild(id, FakeManagedCrud.class, null))
				.thenThrow(new RuntimeException());

		service.read(id, mockUuid);
	}

	@Test
	public <C, I> void createTest() {

		FakeManagedCrudDto fakeCrudDto = new FakeManagedCrudDto();

		when(service.getItemFactory().build(fakeCrudDto, null))
				.thenReturn(mockFake);

		FakeManagedCrudDto u = service.create(fakeCrudDto, mockUuid);

		Assert.assertNotNull(u);
	}

	@Test(expected = ValidationException.class)
	public void createTest_InvalidData() {
		when(
				service.getItemFactory().build(any(FakeManagedCrudDto.class),
						any(ItemPreparer.class))).thenThrow(
				new CrudItemValidationException());

		service.create(new FakeManagedCrudDto(), mockUuid);
	}

	@Test(expected = BadRequestException.class)
	public void createTest_IllegalArgument() {
		service.create(null, mockUuid);
	}

	@Test(expected = BadRequestException.class)
	public void createTest_ReferencedEntityNotFound() {

		when(
				service.getItemFactory().build(any(FakeManagedCrudDto.class),
						any(ItemPreparer.class))).thenThrow(
				new BadRequestException());

		service.create(new FakeManagedCrudDto(), mockUuid);
	}

	@Test(expected = ServiceException.class)
	public void createTest_Error() {
		when(service.getItemFactory().build(any(FakeManagedCrudDto.class)))
				.thenThrow(new RuntimeException());
		service.create(new FakeManagedCrudDto(), mockUuid);
	}

	@Test
	public void updateTest() {
		Long id = new Long(0);
		FakeManagedCrudDto dto = new FakeManagedCrudDto();

		FakeManagedCrud existingC = mock(FakeManagedCrud.class);

		FakeManagedCrud newC = mock(FakeManagedCrud.class);

		when(newC.toDto()).thenReturn(new FakeManagedCrudDto());
		when(service.getItemFactory().build(dto, null)).thenReturn(newC);
		when(service.getItemFactory().findAndBuild(id, FakeManagedCrud.class, null))
				.thenReturn(existingC);

		FakeManagedCrudDto u = service.update(id, dto, mockUuid);

		Assert.assertNotNull(u);
	}

	@Test(expected = NotFoundException.class)
	public void updateTest_NotFound() {
		Long id = new Long(0);
		when(service.getItemFactory().findAndBuild(id, FakeManagedCrud.class, null))
				.thenReturn(null);
		service.update(id, new FakeManagedCrudDto(), mockUuid);
	}

	@Test(expected = ValidationException.class)
	public void updateTest_InvalidData() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						FakeManagedCrud.class, null)).thenThrow(
				new CrudItemValidationException());
		service.update(new Long(0), new FakeManagedCrudDto(), mockUuid);
	}

	@Test(expected = BadRequestException.class)
	public void updateTest_IllegalArg1() {

		service.update(null, new FakeManagedCrudDto(), mockUuid);
	}

	@Test(expected = BadRequestException.class)
	public void updateTest_IllegalArg2() {

		service.update(new Long(123), null, mockUuid);
	}

	@Test(expected = ServiceException.class)
	public void updateTest_Error() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						FakeManagedCrud.class, null)).thenThrow(
				new RuntimeException());
		service.update(new Long(0), new FakeManagedCrudDto(), mockUuid);
	}

	@Test
	public void deleteTest() {
		Long id = new Long(0);
		when(service.getItemFactory().findAndBuild(id, FakeManagedCrud.class, null))
				.thenReturn(mockFake);
		service.delete(id, mockUuid);

	}

	@Test(expected = BadRequestException.class)
	public void deleteTest_NotFound() {

		service.delete(null, mockUuid);

	}

	@Test(expected = ServiceException.class)
	public void deleteTest_Error() {

		when(
				service.getItemFactory().findAndBuild(new Long(0),
						FakeManagedCrud.class, null)).thenThrow(
				new RuntimeException());
		service.delete(new Long(0), mockUuid);

	}

	@Test
	public void getPageTest() {

		when(service.getItemFactory().buildPage(FakeManagedCrud.class, null))
				.thenReturn(mockPage);

		PageOfItemsDto<FakeManagedCrudDto> d = service.getPage(1, 10, mockUuid);

		Assert.assertNotNull(d);
	}

	@Test(expected = AccessUnauthorizedException.class)
	public void getPage_NullToken() {

		service.getPage(1, 10, null);

	}

	@Test(expected = AccessUnauthorizedException.class)
	public void getPage_TokenNotFound() {

		when(
				service.getAccessFactory().findAndBuild(mockUuid,
						AccessToken.class, null)).thenReturn(null);
		service.getPage(1, 10, mockUuid);

	}

	@Test(expected = ServiceException.class)
	public void getPage_ServiceException() {

		when(
				service.getAccessFactory().findAndBuild(mockUuid,
						AccessToken.class, null)).thenThrow(
				new RuntimeException());

		service.getPage(1, 10, mockUuid);

	}

	@Test
	public void getAclTest() {
		when(
				service.getItemFactory().findAndBuild(new Long(123),
						FakeManagedCrud.class, null)).thenReturn(mockFake);

		ACLDto acl = service.getAcl(new Long(123), mockUuid);

		Assert.assertNotNull(acl);

	}

	@Test
	public void updateAcl_Test() {
		when(
				service.getItemFactory().findAndBuild(new Long(123),
						FakeManagedCrud.class, null)).thenReturn(mockFake);

		ACLDto newAclState = new ACLDto();
		newAclState.setOwningUserId(new Long(23456));

		when(mockFake.updateACL(any(ACL.class))).thenReturn(
				new ACL());

		ACLDto acl = service.updateAcl(new Long(123), newAclState, mockUuid);

		Assert.assertNotNull(acl);

	}

	@Test
	public void getAllowedOps_Test() {
		when(
				service.getItemFactory().findAndBuild(new Long(123),
						FakeManagedCrud.class, null)).thenReturn(mockFake);

		when(mockFake.operationsPermitted()).thenReturn(
				new LinkedList<String>());

		OperationListDto allowedOpsDto = service.getAllowedOps(123L, mockUuid);

		Assert.assertNotNull(allowedOpsDto);
		Assert.assertNotNull(allowedOpsDto.getAllowedOperations());

	}

}
