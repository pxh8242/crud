package com.tcbakes.crud.impl.base;

import static org.mockito.Mockito.mock;

import javax.persistence.EntityManager;

public class PseudoCrudService extends
		CrudService<PseudoCrud,PseudoCrudDto,Long> {

	
	public PseudoCrudService() {
		super();
		postContruct();
	}
	
	
	@Override
	public void initialize() {
	}

	@Override
	protected void postContruct() {
		//Create a bunch of Mock locators and entity managers
		this.setTxnAccess(mock(TransactionAccessLocal.class));
		this.setItemFactory(mock(EntityFactory.class));
		this.setItemLocator(mock(Locator.class));
	}
	
	/**
	 * This method simply wraps {@link CrudService#validateToken(String)}. Call
	 * this method from a unit test to excercise the logic in validateToken
	 * 
	 * @param tokenId
	 */
	public void doSomething(String tokenId) {
	}

	@Override
	protected EntityManager getItemEntityManager() {
		return mock(EntityManager.class);
	}

	@Override
	protected Class<PseudoCrud> getManagedClass() {
		// TODO Auto-generated method stub
		return PseudoCrud.class;
	}

}
