package com.tcbakes.crud.impl.access;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, Long> id;
	public static volatile ListAttribute<User, UserGroup> userGroups;
	public static volatile SingularAttribute<User, ACL> acl;
}

