package com.tcbakes.crud.impl.base;

/**
 * Manages a set of contexts by leveraging {@link ThreadLocal} to maintain a
 * unique context per unique thread. All of these methods here will
 * get/set/unset the Context in relation to the currently executing thread
 * 
 * @author Tristan Baker
 * 
 */
public class SimpleThreadLocalContextProvider implements ContextProvider {

	private static final ThreadLocal<ExecutionContext> operationContextStorage = new ThreadLocal<>();

	public void set(ExecutionContext c) {
		operationContextStorage.set(c);
	}

	public void unset() {
		if (operationContextStorage.get() != null)
			operationContextStorage.remove();
	}

	@Override
	public ExecutionContext getContext() {
		if (operationContextStorage.get() == null)
			operationContextStorage.set(new ExecutionContext());

		return operationContextStorage.get();

	}

}
