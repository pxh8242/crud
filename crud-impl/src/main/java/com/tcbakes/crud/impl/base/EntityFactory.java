package com.tcbakes.crud.impl.base;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcbakes.crud.impl.exception.CrudServiceException;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.tools.CrudItemRegistrationException;
import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.NoBackingForDtoException;

/**
 * Creates instances of {@link Crud}, injecting the necessary dependencies
 * <p>
 * Note that a failure to use this EntityFactory and instead doing things like
 * <code>Crud c = new Crud();</code> will result in Crud objects without their
 * required dependencies (eg, {@link Crud#entityManager} and
 * {@link Crud#locator}, etc})
 * 
 * @author Tristan Baker
 * @since 1.1
 */
public class EntityFactory {

	private Locator locator;

	/**
	 * Initializes an EntityFactory with all of the dependencies that will in
	 * turn be provided to the Crud items it builds.
	 * 
	 * @param loc
	 *            A locator
	 * 
	 */
	public EntityFactory(Locator loc) {
		this.locator = loc;
	}

	private static Logger logger = LoggerFactory.getLogger(EntityFactory.class);

	private static final String IdNull = "id cannot be null";
	private static final String ClassToBuildNull = "classToBuild cannot be null";
	private static final String DtoNull = "dto cannot be null";

	private static CrudItemsDirectory directory;

	static {
		directory = new CrudItemsDirectory();
		try {
			directory.initialize();
		} catch (CrudItemRegistrationException e) {
			logger.error("EntityFactory cannot be created because the CrudItemsDirectory failed to initialize.", e);
			throw new RuntimeException(
					"Failed to initialize CrudItemsDirectory.", e);
		}
	}
	
	/**
     * @return The crud item directory
     */
    public CrudItemsDirectory getDirectory() {
        return directory;
    }

	/**
	 * Attempts to build an equivalent {@link Crud} from the state of the
	 * provided {@link Dto}.
	 * <p>
	 * The appropriate Crud to build is retrieved from a registered pair of
	 * equivalent Crud/Dto pairs. Registering a Crud/Dto pair is the
	 * responsibility of the implementor of a specific Crud/Dto type hierarchy
	 * and is achieved, in part, by annotating classes with the
	 * {@link com.tcbakes.crud.tools.ProvidesBackingForDto} annotation.
	 * 
	 * @param dto
	 *            The dto to build the Crud object from
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * 
	 * 
	 * @return An equivalent Crud instance
	 * @throws CrudIllegalArgException
	 *             if dto is null
	 * @throws CrudServiceException
	 *             if the provided dto does not have a registered Crud
	 *             equivalent
	 * @since 1.5
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object build(Object dto, ItemPreparer preparer)
			throws CrudServiceException {

		if (dto == null)
			throw new IllegalArgumentException(DtoNull);

		logger.trace("Building equivalent Crud item for itemType={}", dto
				.getClass().getName());

		Crud t = null;

		try {
			t = (Crud) directory.getBackingInstance(dto.getClass());
		} catch (InstantiationException | IllegalAccessException
				| NoBackingForDtoException e) {
			throw new CrudServiceException(MessageFormat.format(
					"Could not instantiate backing instance for dto: {0}",
					dto.getClass()));
		}
		t.initializeFrom((Dto) dto);

		if (preparer != null)
			preparer.prepare(t);

		return t;

	}

	public Object build(Object dto) throws CrudServiceException {

		return build(dto, null);

	}

	/**
	 * Initializes the dependencies of a provided PageOfItems instance
	 * 
	 * @param classToPage
	 *            the class that will make up the items of this page
	 * @param preparer
	 *            a preparer that will be used to prepare each of the items on
	 *            the page
	 * @return a PageOfItems instance with dependencies prepared
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PageOfItems buildPage(Class classToPage, ItemPreparer preparer) {
		PageOfItems page = new PageOfItems();
		page.setFactory(this);
		page.setPreparer(preparer);
		page.setManagedClass(classToPage);
		page.setLocator(this.locator);
		return page;
	}

	/**
	 * Builds a {@link Crud} from an existing record in the system
	 * 
	 * @param id
	 *            The id of the existing item
	 * @param classToFind
	 *            The class of the item to find. This can be an abstract class
	 *			  and must be a valid JPA Entity
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * @return The item if it exists, null otherwise
	 * @since 1.5
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object findAndBuild(Object id, Class classToFind,
			ItemPreparer preparer) {

		if (id == null)
			throw new IllegalArgumentException(IdNull);

		logger.trace("Finding and Building item. itemType={}, id={}",
				classToFind.getName(), id);

		Crud item = (Crud) locator.findById((Class<Crud>) classToFind, id);

		if (item != null) {

			if (preparer != null)
				preparer.prepare(item);
		}

		return item;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List findAllAndBuild(int pageNum, int pageSize, Class classToBuild,
			ItemPreparer preparer) {

		logger.trace(
				"Finding and Building items. itemType={}, pageNum={}, pageSize={}",
				classToBuild, pageNum, pageSize);

		List items = locator.findAll(pageNum, pageSize, classToBuild);

		for (Object item : items) {

			if (preparer != null)
				preparer.prepare((Crud) item);
		}

		return items;
	}

	/**
	 * Builds a collection of {@link Crud} items representing a subset of all
	 * items available in the system
	 * 
	 * @param classToBuild
	 *            The type of Crud item to build
	 * @param pageNum
	 *            the 1-based page number representing an index into the subset
	 * @param pageSize
	 *            The size of the page
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * 
	 * @return a List of items
	 * 
	 * @since 1.5
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List findAllAndBuild(Class classToBuild, int pageNum, int pageSize,
			ItemPreparer preparer) {

		if (classToBuild == null)
			throw new IllegalArgumentException(ClassToBuildNull);

		logger.trace(
				"Finding and Building items. itemType={}, pageNum={}, pageSize={}",
				classToBuild.getName(), pageNum, pageSize);

		List items = locator.findAll(pageNum, pageSize, classToBuild);

		for (Object item : items) {

			if (preparer != null)
				preparer.prepare((Crud) item);
		}

		return items;

	}

	/**
	 * Builds a single item
	 * 
	 * @param singleItemQuery
	 *            The query to execute
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * @return The item if it exists, null otherwise
	 * 
	 * @since 1.5
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Crud findAndBuildSingleFromQuery(Query singleItemQuery,
			ItemPreparer preparer) {

		if (singleItemQuery == null)
			throw new IllegalArgumentException("singleItemQuery cannot be null");

		Crud c = null;

		try {
			c = (Crud) singleItemQuery.getSingleResult();
		} catch (NoResultException ex) {
		}

		if (c != null) {

			if (preparer != null)
				preparer.prepare(c);
		}

		return c;

	}

	/**
	 * Builds items in a List defined by a provided query
	 * 
	 * @param manyItemQuery
	 *            The query to execute
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * @return The collection of items
	 * 
	 * @since 1.5
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List findAndBuildManyFromQuery(Query manyItemQuery,
			ItemPreparer preparer) {

		if (manyItemQuery == null)
			throw new IllegalArgumentException("manyItemQuery cannot be null");

		List<Crud> cList = (List<Crud>) manyItemQuery.getResultList();

		if (cList != null) {

			for (Crud c : cList) {

				if (preparer != null) {
					preparer.prepare(c);
				}
			}
		}

		return cList;
	}

	/**
	 * Initializes the dependencies of a crud instance
	 * 
	 * @param crud
	 *            The crud object to initialize
	 * @param preparer
	 *            Optional. Delegated to for further item initialization
	 * @since 1.5
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void initialize(Crud crud, ItemPreparer preparer) {

		if (preparer != null) {
			preparer.prepare(crud);
		}
	}
}
