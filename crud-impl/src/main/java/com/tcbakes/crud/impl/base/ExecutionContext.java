package com.tcbakes.crud.impl.base;

import javax.persistence.EntityManager;

import com.tcbakes.crud.impl.access.User;

/**
 * Manages access to a number of objects useful for creating data driven,
 * transaction controlled, user aware operations
 * 
 * @author Tristan Baker
 * 
 */
public class ExecutionContext {

	Locator loc;
	EntityManager em;
	EntityFactory itemFactory;
	EntityFactory accessFactory;
	TransactionAccessLocal txnAccss;
	User actor;
	/**
	 * @return Gets the factory required for creating and accessing items, if
	 *         one is present. Else null
	 */
	public EntityFactory getItemFactory() {
		return itemFactory;
	}

	public void setItemFactory(EntityFactory itemFactory) {
		this.itemFactory = itemFactory;
	}

	/**
	 * @return itemFactory Gets the factory required for creating and accessing
	 *         AccessTokens, if one is present. Else null
	 */
	public EntityFactory getAccessFactory() {
		return accessFactory;
	}

	public void setAccessFactory(EntityFactory accessFactory) {
		this.accessFactory = accessFactory;
	}

	/**
	 * @return Gets the TransactionAccessLocal instance useful for managing
	 *         transaction scope of operations, if one is present. Else, null.
	 */
	public TransactionAccessLocal getTxnAccss() {
		return txnAccss;
	}

	public void setTxnAccss(TransactionAccessLocal txnAccss) {
		this.txnAccss = txnAccss;
	}

	/**
	 * @return The user performing the operation. Else, null.
	 */
	public User getActor() {
		return actor;
	}

	public void setActor(User actor) {
		this.actor = actor;
	}

	/**
	 * @return The EntityManager used for JPA supported access to a persisant
	 *         store, if one is available. Else, null
	 */
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * @return The Locator used for access to common query functionality, if one
	 *         is available. Else, null
	 */
	public Locator getLocator() {
		return loc;
	}

	public void setLocator(Locator loc) {
		this.loc = loc;
	}
}
