package com.tcbakes.crud.impl.base;

/**
 * Defines logic whose transaction scope is controllable by using in conjunction
 * with {@link TransactionWrappedLogic}
 * 
 * @author Tristan Baker
 * 
 */
public interface TransactionWrappedLogic {

	/**
	 * Logic to execute
	 */
	void execute();
}
