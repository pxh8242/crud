package com.tcbakes.crud.impl.access;

/**
 * Permitted operations within the Crud project for Users/Usergroups.
 * 
 * @author ebrandell
 * 
 */
public enum GroupOperation {
	/**
	 * A special permission that represents the superset of READ, UPDATE, DELETE
	 * and CREATE
	 */
	ALL("ALL"),
	/**
	 * The ability to read an item
	 */
	READ("READ"),
	/**
	 * The ability to update an item
	 */
	UPDATE("UPDATE"),
	/**
	 * The ability to delete an item
	 */
	DELETE("DELETE"),
	/**
	 * The global previx used to construct a specific CREATE permisison for a
	 * specific ManagedCrud type
	 */
	CREATEPREFIX("CREATE");

	private final String name;

	private GroupOperation(String name) {
		this.name = name;
	}

	public boolean equalsName(String otherName) {
		return (otherName == null) ? false : name.equals(otherName);
	}

	public String toString() {
		return name;
	}
}
