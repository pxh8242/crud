package com.tcbakes.crud.impl.access;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tcbakes.crud.impl.base.ManagedCrud;
import com.tcbakes.crud.spec.access.UserDto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.BadRequestException;

/**
 * Represents an actor capable of performing operations on CRUD items.
 * 
 * @author Tristan Baker
 */
public abstract class User extends ManagedCrud<User, UserDto, Long> {

	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}

	private Long id;

	private List<UserGroup> userGroups;

	private ACL acl;

	/******************************
	 * Translators
	 ******************************/

	/**
	 * @return The username associated with this User
	 */
	public abstract String getFriendlyUsername();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tcbakes.crud.impl.base.Crud#getId()
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return The set of roles to which the User belongs
	 */
	public List<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> groups) {
		this.userGroups = groups;
	}

	public ACL getAcl() {
		return acl;
	}

	public void setAcl(ACL acl) {
		this.acl = acl;
	}

	/**
	 * Returns a list containing all of the possible CREATE operations this user
	 * can perform based on their UserGroups.
	 * 
	 * @return A list of CREATE operations this user can perform
	 */
	public List<String> getCreateOperations() {
		List<String> result = new ArrayList<String>();
		final String prefix = "CREATE_.*";

		if (this.getUserGroups() != null) {
			for (UserGroup ug : this.getUserGroups()) {
				for (String operation : ug.getOperations()) {
					if (operation.matches(prefix)
							&& !result.contains(operation)) {
						result.add(operation);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Initializes all the properties of a User from the provided dto
	 * 
	 * @param dto
	 *            The dto to use for initialization
	 * @throws CrudIllegalArgException
	 *             If any portion of the state of the dto is not usable for
	 *             initialization
	 */
	protected void initialize(UserDto dto) throws BadRequestException {
		this.id = dto.getId();

		if (dto.getUserGroups() != null) {
			this.userGroups = new LinkedList<UserGroup>();
			
            // Remove duplicates from Groups
            HashSet<String> groupSet = new HashSet<String>();
            groupSet.addAll(dto.getUserGroups());
			
			for (String rName : groupSet) {
				UserGroup r = getContext().getLocator().findSingleByFieldValue(
						UserGroup.class, UserGroup_.name, rName);
				if (r == null)
					throw new BadRequestException(
							String.format(
									"User item references role that does not exist. Role='%s'",
									rName));
				this.userGroups.add(r);
			}
		}
	}

    /**
     * Checks that the actor is permitted to perform the Update operation as
     * determined by the state of {@link User#getAcl()}
     * 
     * @see com.tcbakes.crud.impl.base.Crud#doUpdateLogic(com.tcbakes.crud.impl.base.Crud,
     *      com.tcbakes.crud.impl.access.User)
     */
    @Override
    public void doUpdateLogic(User newState) {
        super.doUpdateLogic(newState);
        
        // The incoming newState will not have an ACL because clients aren't
        // permitted to update an ACL through the User.
        // Ensure that the newState references the existing ACL.
        newState.setAcl(this.getAcl());
    }
	
	/**
     * Checks that the actor is permitted to perform the Delete operation as
     * determined by the state of {@link User#getAcl()}
     * 
     * In order for a user to be deleted, the following must be performed <i>in
     * order</i>:
     * 
     * <ol>
     * <li>The ACL for the User being deleted must have any foreign keys to this
     * User nullified and merged</li>
     * <li>The User must have the foreign key for its ACL nullified and merged</li>
     * <li>The ACL must then be deleted</li>
     * <li>The User must be deleted</li>
     * </ol>
     * 
     * @see com.tcbakes.crud.impl.base.Crud#doDeleteLogic(com.tcbakes.crud.impl.access.User)
     */
    @Override
    public void doDeleteLogic() {
        super.doDeleteLogic();

        // Find any AccessToken associated with this user
        List<AccessToken> tokens = getContext().getLocator().findManyByFieldValue(
                AccessToken.class, AccessToken_.user, this);

        // Delete any tokens associated with this user
        for (AccessToken token : tokens) {
            token.delete();
        }

        if (this.getAcl() != null) {
            // Check to see if the user being deleted owns ACLs for
            // any other CRUD items
            @SuppressWarnings("unchecked")
            List<ACL> acls = getContext().getEm()
                    .createNamedQuery("ACL.findAllAclsForUser")
                    .setParameter("owning_user_id", this.getId())
                    .setParameter("acl_id", this.getAcl().getId())
                    .getResultList();

            if (acls.size() > 0) {
                // TODO: Show the user exactly what the specified user
                // owns!
                throw new AccessForbiddenException(
                        MessageFormat
                                .format("Cannot delete user: user owns multiple ACL-managed items. User=''{0}''",
                                        this.getId()));
            }

            // Delete the acl associated with this user.
            this.getAcl().delete();
        }
    }
	
	/**
	 * Translates all the properties of a User to the equivalent properties of
	 * the dto
	 * 
	 * @param dto
	 *            The dto to translate to
	 */
	protected void to(UserDto dto) {
		dto.setId(this.id);
		if (this.getUserGroups() != null) {
			dto.setUserGroups(new LinkedList<String>());
			for (UserGroup r : this.userGroups) {
				dto.getUserGroups().add(r.getName());
			}
		}
	}

	/**
	 * Unlike READ, UPDATE and DELETE operations, the ability to CREATE an
	 * object is determined through an examination of the acting user. This
	 * method will examine all of the UserGroups to which this user belongs and
	 * look for a create permission that matches the incoming argument
	 * 
	 * @param operationName
	 *            The create permission check
	 * @return true if the create permission is present in at least one of the
	 *         user's usergroup's operations collection, false otherwise
	 */
	public boolean createOperationPermitted(String operationName) {
		if (StringUtils.isEmpty(operationName))
			throw new IllegalArgumentException("operationName cannot be empty");

		if (this.getUserGroups() != null) {
			for (UserGroup g : this.getUserGroups()) {
				if (g.getOperations().contains(operationName)) {
					return true;
				}
			}
		}

		return false;
	}
}
