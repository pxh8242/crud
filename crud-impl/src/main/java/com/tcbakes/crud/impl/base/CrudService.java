package com.tcbakes.crud.impl.base;

import javax.persistence.EntityManager;

import com.tcbakes.crud.impl.exception.CrudItemNotFoundException;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

/**
 * A service that provides CRUD functionality for managing a {@link Crud} type.
 * Implementing classes have the option of inheriting the standard method
 * implementations or overriding them to provide variations in behavior.
 * <p>
 * <ul>
 * <li>a {@link Locator} for easy execution of simple queries for items in the
 * system.</li>
 * <li>a {@link EntityFactory} for easy construction of Crud items given a Dto
 * item</li>
 * <li>a {@link TransactionAccessLocal} for controlling the transactional scope
 * of operations</li>
 * </ul>
 * 
 * @param <C>
 *            The explicit {@link Crud} type that is manageable
 * @param <D>
 *            The equivalent DTO type that is exposed to clients of this
 *            service.
 * @param <I>
 *            The type used as the primary identifier
 * @author Tristan Baker
 */
public abstract class CrudService<C extends Crud<C, D, I>, D extends Dto<I>, I>
		extends AbstractService {

	private EntityFactory itemFactory;
	private Locator itemLocator;

	public EntityFactory getItemFactory(){
		return itemFactory;
	}
	
	protected void setItemFactory(EntityFactory fac){
		itemFactory = fac;
	}
	
	public Locator getItemLocator(){
		return itemLocator;
	}
	
	protected void setItemLocator(Locator loc){
		itemLocator = loc;
	}
	
	/**
	 * Retrieve the EntityManager used for persistence management of the Items
	 * managed by this Service. (This may return the same instance as the
	 * {@link CrudService#getAccessEntityManager()})
	 */
	protected abstract EntityManager getItemEntityManager();

	/**
	 * An ItemPreparer to perform initialization activities on a Crud item. This
	 * item preparer will be delegated to after the item has been created, but
	 * before the action is delegated to the item to be performed. By default,
	 * this implementation will return null. Overriding implementations can
	 * optionally return an instance of an ItemPreparer for the item managed by
	 * this service
	 * 
	 */
	protected ItemPreparer<C, D, I> getPreparer() {
		return null;
	}

	/**
	 * Perform custom initialization operations on this service prior to this
	 * service being made available for use
	 * <p>
	 * <strong> HINT: If you are running in an EJB container, annotate this
	 * method with javax.annotation.PostConstruct and call
	 * {@link CrudService#postContruct()}</strong>
	 */
	public abstract void initialize();

	/**
	 * This method must be called prior to this service being available for use.
	 */
	protected void postContruct() {

		this.itemLocator = new Locator(getItemEntityManager());
		this.itemFactory = new EntityFactory(this.itemLocator);

	}

	/*
	 * Error messages for common failure scenarios detectable in the CRUD
	 * methods of this class.
	 */
	protected static final String IdNull = "id cannot be null";
	protected static final String CreateItemNull = "item to create cannot be null";
	protected static final String UpdateItemNull = "item to update cannot be null";

	/**
	 * Retrieve a {@link Dto} by primary Id
	 * 
	 * @param id
	 *            The id of the item to retrieve
	 * @return The item if found.
	 * 
	 * @throws BadRequestException
	 *             If the id is null
	 * @throws NotFoundException
	 *             If the item reference by the id is not found
	 * @throws ServiceException
	 *             Any other error
	 */
	public D read(final I id) throws BadRequestException,
			NotFoundException, AccessForbiddenException, ServiceException {
		
		return wrapAndExecute(new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing read request. itemType={}, id={}",
						getManagedClass().getName(), id);

				if (id == null)
					throw new BadRequestException(IdNull);
				
				return doReadLogic(id);
			}
		});
	}

	/**
	 * Do the read by delegating to the item being updated. This is called as
	 * part of {@link CrudService#read(Object, String)} after the token has been
	 * validated
	 * 
	 * @param id
	 *            The ID of the item to read
	 * @return The item to read
	 * @throws BadRequestException
	 *             If any required parameters are missing
	 * @throws NotFoundException
	 *             If the item referenced by the id is not found
	 * @throws AccessForbiddenException
	 *             If the user is not permitted to perform the operation
	 * @throws ServiceException
	 *             Any other error
	 */
	protected D doReadLogic(I id) throws BadRequestException,
			NotFoundException, AccessForbiddenException, ServiceException {

		@SuppressWarnings("unchecked")
		C item = (C) getContext().getItemFactory().findAndBuild(id, getManagedClass(),
				getPreparer());

		if (item == null)
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());

		item.read();

		return item.toDto();
	}

	/**
	 * Create a new {@link Dto}
	 * 
	 * @param d
	 *            The {@link Dto} to create
	 * @return The created item
	 * @throws BadRequestException
	 *             If d is null
	 * @throws NotFoundException
	 *             If d references another item that is not found
	 * @throws ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws ServiceException
	 *             Any other error
	 */
	public D create(final D d) throws BadRequestException,
			NotFoundException, ValidationException, ServiceException{
		
		return wrapAndExecute(new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing create request. itemType={}",
						getManagedClass().getName());

				if (d == null)
					throw new BadRequestException(CreateItemNull);
				
				return doCreateLogic(d);
			}
		});
	}

	/**
	 * Do the create by delegating to the item being updated. This is called as
	 * part of {@link CrudService#create(Dto, String)} after the token has been
	 * validated
	 * 
	 * @param d
	 *            The item to create
	 * @return The created item
	 * @throws BadRequestException
	 *             If required parameters are missing
	 * @throws NotFoundException
	 *             If d references another item that is not found
	 * @throws com.tcbakes.crud.spec.exception.ValidationException
	 *             If d is not valid as determined by system validation rules
	 * @throws AccessForbiddenException
	 *             If the user is not permitted to perform the operation
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	protected D doCreateLogic(D d) throws BadRequestException,
			NotFoundException,
			com.tcbakes.crud.spec.exception.ValidationException,
			AccessForbiddenException, ServiceException {

		C item = (C) getContext().getItemFactory().build(d, getPreparer());

		item.create();

		return (D) item.toDto();
	}

	/**
	 * Updates the state of a {@link Dto}.
	 * 
	 * @param id
	 *            The primary id of the item to update
	 * @param d
	 *            The new state for the item
	 * @return The updated item
	 * @throws BadRequestException
	 *             If id or d are null
	 * @throws NotFoundException
	 *             If the item identified by id is not found or d references
	 *             another item that is not found
	 * @throws ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws ServiceException
	 *             All other errors
	 */
	public D update(final I id, final D d)
			throws BadRequestException, NotFoundException, ValidationException,
			ServiceException{
		
		return wrapAndExecute(new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing update request. itemType={}, id={}",
						getManagedClass().getName(), id);
				
				if (id == null)
					throw new BadRequestException(IdNull);

				if (d == null)
					throw new BadRequestException(UpdateItemNull);
				
				return doUpdateLogic(id, d);
			}

		});
	}

	/**
	 * Do the update by delegating to the item being updated. This is called as
	 * part of {@link CrudService#update(Object, Dto, String)} after the token
	 * has been validated
	 * 
	 * @param id
	 *            The id of the item to update
	 * @param d
	 *            The new state of the item
	 * @return The new state of the item
	 * @throws BadRequestException
	 *             If any of the arguments are null
	 * @throws NotFoundException
	 *             If the item identified by id is not found or d contains a
	 *             refernce to another item that is not found
	 * @throws com.tcbakes.crud.spec.exception.ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws ServiceException
	 *             Any other problem
	 */
	protected D doUpdateLogic(final I id, final D d)
			throws BadRequestException, NotFoundException,
			com.tcbakes.crud.spec.exception.ValidationException,
			ServiceException {
		

		@SuppressWarnings("unchecked")
		C existingItem = (C) getContext().getItemFactory().findAndBuild(id, getManagedClass(),
				getPreparer());

		if (existingItem == null)
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());

		@SuppressWarnings("unchecked")
		C newItem = (C) getContext().getItemFactory().build(d, getPreparer());

		existingItem.update(newItem);

		return newItem.toDto();
	}

	/**
	 * Removes the {@link Dto} from the system
	 * 
	 * @param id
	 *            The id of the item to remove
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @throws BadRequestException
	 *             If id is null
	 * @throws ServiceException
	 *             All other errors
	 */
	public void delete(final I id) throws BadRequestException,
			ServiceException{
		
		wrapAndExecute(new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing delete request. itemType={}, id={}",
						getManagedClass().getName(), id);

				if (id == null)
					throw new BadRequestException(IdNull);
				
				doDeleteLogic(id);

				return null;
			}

		});
	}

	/**
	 * Do the deletion by delegating to the item being deleted. This is called
	 * as part of {@link CrudService#delete(Object, String)} after the token has
	 * been validated
	 * 
	 * @param id
	 *            The id of the item to delete
	 * @param token
	 *            A valid token
	 * @throws CrudIllegalArgException
	 *             If required parameters are null
	 */
	protected void doDeleteLogic(I id) throws BadRequestException {


		@SuppressWarnings("unchecked")
		C existingItem = (C) getContext().getItemFactory().findAndBuild(id, getManagedClass(),
				getPreparer());

		if (existingItem != null) {
			existingItem.delete();
		} else {
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());
		}
	}

	/**
	 * Retrieves a subset of all the items in the system. Note that if the
	 * subset contains items that the user is not permitted to view, they will
	 * be "redacted" and no effort will be made to fill the page with suitable
	 * replacement records.
	 * 
	 * 
	 * @param pageNum
	 *            The page to retrieve
	 * @param pageSize
	 *            The size of the page
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return A subset of items in the system.
	 * @throws BadRequestException
	 *             If required arguments are missing or if the index into the
	 *             subset represented by pageNum falls outside the bounds of the
	 *             set
	 * @throws ServiceException
	 *             any other problem
	 */
	public PageOfItemsDto<D> getPage(final int pageNum,
			final int pageSize)
			throws BadRequestException, ServiceException{
		
		return wrapAndExecute(new ExceptionHandledLogic<PageOfItemsDto<D>>() {

			@Override
			public PageOfItemsDto<D> execute() {
				logger.debug(
						"Processing page request. itemType={}, pageNum={}, pageSize={}",
						getManagedClass().getName(), pageNum, pageSize);

				return doPageLogic(pageNum, pageSize);

			}
		});
	}

	/**
	 * Retrieve the actual page of items. This is called as part of
	 * {@link CrudService#getPage(int, int, String)} after the token has been
	 * validated
	 * 
	 * @param pageNum
	 *            The page to retrieve
	 * @param pageSize
	 *            The size of the page
	 * @param actor
	 *            The user performing the operation
	 * @return A subset of items in the system
	 * @throws BadRequestException
	 *             If required arguments are missing or if the index into the
	 *             subset represented by pageNum falls outside the bounds of the
	 *             set
	 * @throws AccessForbiddenException
	 *             If the user is not permitted to perform the action
	 * @throws ServiceException
	 *             Any other problem
	 */
	protected PageOfItemsDto<D> doPageLogic(int pageNum, int pageSize)
			throws BadRequestException, AccessForbiddenException,
			ServiceException {

		@SuppressWarnings("unchecked")
		PageOfItems<C, D, I> page = getContext().getItemFactory().buildPage(getManagedClass(),
				getPreparer());
		
		page.read(pageNum, pageSize);

		return page.toDto();
	}

	/* (non-Javadoc)
	 * @see com.tcbakes.crud.impl.base.AbstractService#wrapAndExecute(com.tcbakes.crud.impl.base.AbstractService.ExceptionHandledLogic)
	 */
	@Override
	protected <T> T wrapAndExecute(ExceptionHandledLogic<T> l) {

		try {

			getContextProvider().getContext().setEm(getItemEntityManager());
			getContextProvider().getContext().setItemFactory(this.itemFactory);
			getContextProvider().getContext().setLocator(this.itemLocator);
			
			return super.wrapAndExecute(l);

		} finally {
			getContextProvider().unset();
		}

	}

	/**
	 * Extending classes will provide an implementation of this method and
	 * return the class that the service manages. The Class need not be explicit
	 * (ie, non-abstract) as long as it is a direct descendant of a {@link Crud}
	 * 
	 * @return The {@link Crud} class managed by this service.
	 */
	protected abstract Class<C> getManagedClass();
}
