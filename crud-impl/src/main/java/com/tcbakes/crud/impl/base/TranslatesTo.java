package com.tcbakes.crud.impl.base;

/**
 * Capable of translating itself to D
 * 
 * @param <D>
 *            The target type of the translation
 * 
 * @author Tristan Baker
 */
public interface TranslatesTo<D> {

	/**
	 * Translate to the target type
	 * 
	 * @return The initilized target type
	 */
	public D toDto();
}
