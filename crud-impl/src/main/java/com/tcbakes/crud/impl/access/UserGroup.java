package com.tcbakes.crud.impl.access;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.PostLoad;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import com.tcbakes.crud.impl.base.ManagedCrud;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.impl.exception.ValidationFailures;
import com.tcbakes.crud.spec.access.UserGroupDetailsDto;
import com.tcbakes.crud.spec.access.UserGroupDto;
import com.tcbakes.crud.tools.ManagedCrudOperation;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

/**
 * A Role identifies the set of operations that a user is allowed to perform
 * 
 * @author tbaker
 * 
 */
@ProvidesBackingForDto(UserGroupDto.class)
@ManagedCrudOperation({"CREATE_USERGROUP"})
public class UserGroup extends ManagedCrud<UserGroup, UserGroupDto, Long> {

    private static final long serialVersionUID = 1L;

    public UserGroup() {
        super();
    }

    Long id;

    String name;

    String operationStr;

    List<String> operations;

    List<ACL> acls;
    
    ACL acl;
    
    /*
     * (non-Javadoc)
     * 
     * @see com.tcbakes.crud.impl.base.Crud#getId()
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getOperationStr() {
        return operationStr;
    }

    public void setOperationStr(String operationStr) {
        this.operationStr = operationStr;
    }

    public List<String> getOperations() {
        return operations;
    }

    public void setOperations(List<String> operations) {
        this.operations = operations;
    }

    public List<ACL> getAcls() {
        return acls;
    }

    public void setAcls(List<ACL> acls) {
        this.acls = acls;
    }

    public ACL getAcl() {
        return acl;
    }

    public void setAcl(ACL acl) {
        this.acl = acl;
    }

    /**
     * @param name
     *            The name of this Role
     */
    public void setName(String name) {
        this.name = name;
    }
    
    private ValidationFailures validationLogic(UserGroup userGroup) {
   
        ValidationFailures f = new ValidationFailures();

        if (StringUtils.isEmpty(userGroup.getName()))
            f.addValidationError("name", "name cannot be empty");
        
        if (userGroup.getOperations() == null || userGroup.getOperations().isEmpty())
            f.addValidationError("operations", "operations cannot be empty");
        
        if (userGroup.getOperations() != null) {
            for (String ops : userGroup.getOperations()) {
                if (!EnumUtils.isValidEnum(GroupOperation.class, ops)
                        && !(this.getContext().getItemFactory().getDirectory()
                                .getValidOperations()).contains(ops)) {
                    f.addValidationError("operation", "operation '" + ops
                            + "' not found!");
                }
            }
        }
        
        return f;
    }

    @Override
    public void doCreateLogic() {
    	super.doCreateLogic();
    	
        ValidationFailures f = validationLogic(this);

        UserGroup collidingRole = getContext().getLocator().findSingleByFieldValue(
                UserGroup.class, UserGroup_.name, this.getName());

        if (collidingRole != null) {
            f.addValidationError("name", "name must be unique"); 
            f.setConflicts(true);
        }

        if (f.hasFailures() && f.isConflicts()) {
            throw new CrudItemValidationException(f, Status.CONFLICT);
        } else if(f.hasFailures() && !f.isConflicts()){
            throw new CrudItemValidationException(f);
        }

        
        // Create default ACL 
        ACL acl = new ACL();
        getContext().getItemFactory().initialize(acl, null);
        acl.setOwningUser(getContext().getActor());
        this.setAcl(acl);
    }

    @Override
    public void doUpdateLogic(UserGroup newState) {
    	super.doUpdateLogic(newState);
    	
        ValidationFailures f = validationLogic(newState);
        
        UserGroup r = getContext().getLocator().findSingleByFieldValue(UserGroup.class,
                UserGroup_.name, newState.getName());

        if (r != null && !r.getId().equals(this.getId())) {
            f.addValidationError("name", "name must be unique");
            f.setConflicts(true);
        }
        
        // Copy ACL from the current ACL
        newState.setAcl(this.getAcl());

        if (f.hasFailures() && f.isConflicts()) {
            throw new CrudItemValidationException(f, Status.CONFLICT);
        } else if(f.hasFailures() && !f.isConflicts()){
            throw new CrudItemValidationException(f);
        }
    }

    @Override
    public UserGroupDto toDto() {
        UserGroupDto dto = new UserGroupDto();

        dto.setId(this.getId());
        dto.setName(this.getName());

        if (this.operations != null) {
            dto.setOperations(new ArrayList<String>());

            for (String o : this.operations) {
                dto.getOperations().add(o);
            }
        }

        return dto;
    }

    @Override
    public void initializeFrom(UserGroupDto dto) {
        this.id = dto.getId();
        this.name = StringUtils.strip(dto.getName());

        if (dto.getOperations() != null) {
            this.operations = new ArrayList<String>();

            // Remove duplicates from Groups
            HashSet<String> operationsSet = new HashSet<String>();
            operationsSet.addAll(dto.getOperations());
            
            for (String s : operationsSet) {
                this.operations.add(s.toUpperCase());
            }
        }

        if (this.getOperations() != null) {
            this.setOperationStr(this.convertToString());
        }
    }

    public String convertToString() {
        StringBuilder b = new StringBuilder();

        for (String p : operations) {
            b.append(p.toString());
            b.append(",");
        }

        if (b.length() > 0) {
            return b.toString().substring(0, b.length() - 1);
        } else {
            return b.toString();
        }
    }

    public void buildFromString(String v) {
        if (!StringUtils.isEmpty(v)) {

        	this.operations = new ArrayList<String>();

            String[] strings = v.split(",");
 
            for (String s : strings) {
                this.operations.add(s.toUpperCase());
            }
        }
    }

    public UserGroupDetailsDto toDetailsDto() {
        UserGroupDetailsDto dto = new UserGroupDetailsDto();

        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setOperations(this.getOperations());

        @SuppressWarnings("unchecked")
        List<User> users = getContext().getEm()
                .createNamedQuery("User.findUsersByUserGroupId")
                .setParameter("id", this.getId()).getResultList();

        dto.setMembers(new HashMap<String, Long>());
        for (User user : users) {
            dto.getMembers().put(user.getFriendlyUsername(), user.getId());
        }

        return dto;
    }

    @PostLoad
    public void postLoad() {
        if (StringUtils.isNotBlank(this.operationStr)) {
            buildFromString(this.operationStr);
        }
    }
}
