package com.tcbakes.crud.impl.base;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.collections.CollectionUtils;

import com.tcbakes.crud.impl.exception.CrudServiceException;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.exception.BadRequestException;

/**
 * Locates items in the persistent store. This class is intended for definition
 * and execution of simple queries. More complex/specialize querying needs will
 * not be supported here
 * 
 * @author Tristan Baker
 */
public class Locator {

	private EntityManager em;

	public Locator(EntityManager em){
		this.em = em;
	}

	/**
	 * Finds a {@link Crud} by its primary id
	 * 
	 * 
	 * @param classToFind
	 *            The type of item to find
	 * 
	 * @param id
	 *            The primary id
	 * @return The object if found, otherwise <code>null</code>
	 */
	public <C, I> C findById(Class<C> classToFind, I id) {
		if (id == null)
			throw new IllegalArgumentException("id cannot be null");

		C entity = em.find(classToFind, id);

		return entity;
	}

	/**
	 * Finds a collection of items by a single field valye
	 * 
	 * @param entityToFind
	 *            The entity to find
	 * @param field
	 *            The field to compare against
	 * @param value
	 *            The value to check for equality
	 * @return The collection of items. Potentially empty if no matching results
	 *         found.
	 */
	public <C, F> List<C> findManyByFieldValue(Class<C> entityToFind,
			SingularAttribute<? super C, F> field, Object value) {

		CriteriaQuery<C> criteria = em.getCriteriaBuilder().createQuery(
				entityToFind);
		Root<C> coverage = criteria.from(entityToFind);
		criteria.select(coverage);
		criteria.where(em.getCriteriaBuilder()
				.equal(coverage.get(field), value));

		List<C> results = em.createQuery(criteria).getResultList();

		return results;
	}

	/**
	 * Finds a sing item by a single field value
	 * 
	 * @param entityToFind
	 *            The entity to find
	 * @param field
	 *            The field to compare against
	 * @param value
	 *            The value to check
	 * @return The item if found, null otherwise.
	 */
	public <C, F> C findSingleByFieldValue(Class<C> entityToFind,
			SingularAttribute<? super C, F> field, Object value) {
		CriteriaQuery<C> criteria = em.getCriteriaBuilder().createQuery(
				entityToFind);
		Root<C> item = criteria.from(entityToFind);
		criteria.select(item);
		criteria.where(em.getCriteriaBuilder().equal(item.get(field), value));

		List<C> results = em.createQuery(criteria).getResultList();

		if (CollectionUtils.isEmpty(results))
			return null;
		else if (results.size() > 1)
			throw new CrudServiceException(
					"Found multiple results when only one was expected");

		return results.get(0);
	}

	/**
	 * Retrieves a subset of the available records in the system
	 * 
	 * @param pageNum
	 *            Which of the available pages to return
	 * @param pageSize
	 *            The number of records per page.
	 * @param entityClass
	 *            The kind of item to find.
	 * @return A page of records
	 */
	public <C extends Crud<C, D, I>, D extends Dto<I>, I> List<C> findAll(
			int pageNum, int pageSize, Class<C> entityClass) {

		if (pageSize <= 0)
			throw new BadRequestException(String.format(
					"Page size of %s is not valid. Must be larger than 0.",
					pageSize));

		if (pageNum < 1)
			throw new BadRequestException(String.format(
					"Page number %s does not exist. Page numbers start at 1",
					pageNum));

		int numberOfRecords = count(entityClass);
		int firstResult = ((pageNum - 1) * pageSize);

		int totalNumberOfPages = numberOfRecords % pageSize == 0 ? (numberOfRecords / pageSize)
				: (numberOfRecords / pageSize) + 1;
		
		
		if ((totalNumberOfPages != 0 ) && pageNum > totalNumberOfPages)
		{
			System.out.println("totalNumberOfPages   " +  totalNumberOfPages);
			throw new BadRequestException(
					String.format(
							"Page number %s does not exist.  Highest allowable page number of size %s is %s.",
							pageNum, pageSize, totalNumberOfPages));

		}
		
		PageOfItems<C, D, I> page = new PageOfItems<>();
		page.setPageItems(new LinkedList<C>());

		CriteriaQuery<C> criteria = em.getCriteriaBuilder().createQuery(
				entityClass);
		Root<C> configRoot = criteria.from(entityClass);
		criteria.select(configRoot);

		TypedQuery<C> query = em.createQuery(criteria);
		query.setFirstResult(firstResult);
		query.setMaxResults(pageSize);

		List<C> entities = query.getResultList();

		return entities;
	}

	/**
	 * Provides a count of the current number of records
	 * 
	 * @param entityClass
	 *            The explicit entity to count
	 * @return The count of the number of records
	 */
	public <C> int count(Class<C> entityClass) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> query = cb.createQuery(Long.class);
		Root<C> root = query.from(entityClass);
		query.select(cb.count(root));
		Long count = em.createQuery(query).getSingleResult();

		return count.intValue();
	}
}
