package com.tcbakes.crud.impl.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Item {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

	private ContextProvider provider = null;
	
	protected ContextProvider getContextProvider(){
		if(provider == null)
			provider = new SimpleThreadLocalContextProvider();
		
		return provider;
	}
	
	protected ExecutionContext getContext(){
		return getContextProvider().getContext();
	}
}
