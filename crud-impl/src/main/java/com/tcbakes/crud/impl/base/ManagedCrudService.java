package com.tcbakes.crud.impl.base;

import java.util.List;

import javax.persistence.EntityManager;

import com.tcbakes.crud.impl.access.ACL;
import com.tcbakes.crud.impl.access.AccessToken;
import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.impl.exception.CrudItemNotFoundException;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.access.ACLDto;
import com.tcbakes.crud.spec.access.OperationListDto;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.base.ManagedDto;
import com.tcbakes.crud.spec.exception.AbstractSpecException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.AccessUnauthorizedException;
import com.tcbakes.crud.spec.exception.AllExcpetionsMapper;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;
import com.tcbakes.crud.spec.exception.ValidationExceptionConflict;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

public abstract class ManagedCrudService<C extends ManagedCrud<C, D, I>, D extends ManagedDto<I>, I>
		extends CrudService<C, D, I> {

	private EntityFactory accessFactory;

	public EntityFactory getAccessFactory(){
		return accessFactory;
	}
	
	protected void setAccessFactory(EntityFactory fac){
		accessFactory = fac;
	}
	
	@Override
	protected void postContruct() {
		super.postContruct();
		Locator accessLocator = new Locator(getAccessEntityManager());
		this.accessFactory = new EntityFactory(accessLocator);
	}

	/**
	 * Retrieve the EntityManager used for persistence management of
	 * AccessTokens
	 */
	protected abstract EntityManager getAccessEntityManager();

	/**
	 * Retrieve a {@link Dto} by primary Id
	 * 
	 * @param id
	 *            The id of the item to retrieve
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The item if found.
	 * 
	 * @throws BadRequestException
	 *             If the id is null
	 * @throws NotFoundException
	 *             If the item reference by the id is not found
	 * @throws AccessForbiddenException
	 *             If the user is not permitted to perform the operation
	 * @throws ServiceException
	 *             Any other error
	 */
	public D read(final I id, final String tokenId) throws BadRequestException,
			NotFoundException, AccessForbiddenException, ServiceException {

		return authWrapAndExecute(tokenId, new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {

				logger.debug(
						"Processing read request. itemType={}, id={}, accessToken={}",
						getManagedClass().getName(), id, tokenId);

				if (id == null)
					throw new BadRequestException(IdNull);
				
				return doReadLogic(id);
			}
		});
	}

	/**
	 * Create a new {@link Dto}
	 * 
	 * @param d
	 *            The {@link Dto} to create
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The created item
	 * @throws BadRequestException
	 *             If d is null
	 * @throws NotFoundException
	 *             If d references another item that is not found
	 * @throws com.tcbakes.CrudItemValidationException.service.exception.ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws ServiceException
	 *             Any other error
	 */
	public D create(final D d, final String tokenId)
			throws BadRequestException, NotFoundException,
			com.tcbakes.crud.spec.exception.ValidationException,
			AccessForbiddenException, ServiceException {

		return authWrapAndExecute(tokenId, new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing create request. itemType={}, accessToken={}",
						getManagedClass().getName(), tokenId);

				if (d == null)
					throw new BadRequestException(CreateItemNull);
				
				return doCreateLogic(d);
			}
		});
	}

	/**
	 * Updates the state of a {@link Dto}.
	 * 
	 * @param id
	 *            The primary id of the item to update
	 * @param d
	 *            The new state for the item
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The updated item
	 * @throws BadRequestException
	 *             If id or d are null
	 * @throws NotFoundException
	 *             If the item identified by id is not found or d references
	 *             another item that is not found
	 * @throws com.tcbakes.CrudItemValidationException.service.exception.ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws ServiceException
	 *             All other errors
	 */
	public D update(final I id, final D d, final String tokenId)
			throws BadRequestException, NotFoundException,
			com.tcbakes.crud.spec.exception.ValidationException,
			AccessForbiddenException, ServiceException {

		return authWrapAndExecute(tokenId, new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing update request. itemType={}, id={}, accessToken={}",
						getManagedClass().getName(), id, tokenId);

				if (id == null)
					throw new BadRequestException(IdNull);

				if (d == null)
					throw new BadRequestException(UpdateItemNull);
				
				return doUpdateLogic(id, d);
			}

		});
	}

	
	/* (non-Javadoc)
	 * @see com.tcbakes.crud.impl.base.CrudService#doUpdateLogic(java.lang.Object, com.tcbakes.crud.spec.base.Dto)
	 */
	@Override
	protected D doUpdateLogic(final I id, final D d)
			throws BadRequestException, NotFoundException,
			com.tcbakes.crud.spec.exception.ValidationException,
			ServiceException {

		@SuppressWarnings("unchecked")
		C existingItem = (C) getContext().getItemFactory().findAndBuild(id,
				getManagedClass(), getPreparer());

		if (existingItem == null)
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());

		@SuppressWarnings("unchecked")
		C newItem = (C) getContext().getItemFactory().build(d, getPreparer());

		// The incoming newState will not have an ACL because clients aren't
		// permitted to update an ACL through the ManagedCrud item.
		// Ensure that the newState references the existing ACL.
		// NOTE: this is the reason a custom implementation of this method is needed
		// rather than being able to rely on the one in the parent class.
		newItem.setAcl(existingItem.getAcl());

		existingItem.update(newItem);

		return newItem.toDto();
	}

	
	
	/**
	 * Removes the {@link Dto} from the system
	 * 
	 * @param id
	 *            The id of the item to remove
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @throws BadRequestException
	 *             If id is null
	 * @throws ServiceException
	 *             All other errors
	 */
	public void delete(final I id, final String tokenId)
			throws BadRequestException, AccessForbiddenException,
			ServiceException {

		authWrapAndExecute(tokenId, new ExceptionHandledLogic<D>() {

			@Override
			public D execute() {
				logger.debug(
						"Processing delete request. itemType={}, id={}, accessToken={}",
						getManagedClass().getName(), id, tokenId);

				if(id == null)
					throw new BadRequestException(IdNull);
				
				doDeleteLogic(id);

				return null;
			}

		});

	}

	/**
	 * Retrieves a subset of all the items in the system. Note that if the
	 * subset contains items that the user is not permitted to view, they will
	 * be "redacted" and no effort will be made to fill the page with suitable
	 * replacement records.
	 * 
	 * 
	 * @param pageNum
	 *            The page to retrieve
	 * @param pageSize
	 *            The size of the page
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return A subset of items in the system.
	 * @throws BadRequestException
	 *             If required arguments are missing or if the index into the
	 *             subset represented by pageNum falls outside the bounds of the
	 *             set
	 * @throws AccessForbiddenException
	 *             If the user is not permitted to perform the action
	 * @throws ServiceException
	 *             any other problem
	 */
	public PageOfItemsDto<D> getPage(final int pageNum, final int pageSize,
			final String tokenId) throws BadRequestException,
			AccessForbiddenException, ServiceException {

		return authWrapAndExecute(tokenId,
				new ExceptionHandledLogic<PageOfItemsDto<D>>() {

					@Override
					public PageOfItemsDto<D> execute() {
						logger.debug(
								"Processing page request. itemType={}, pageNum={}, pageSize={}, accessToken={}",
								getManagedClass(), pageNum, pageSize, tokenId);

						return doPageLogic(pageNum, pageSize);
					}

				});
	}

	/**
	 * By default, this method will return an instance of {@link PageOfItems}
	 * whose read implementation will read records from the system's persistent
	 * store. Overriders of this method may wish to return an instance that
	 * reads items from some other sources. This can be achieved by also
	 * extending the default {@link PageOfItems} implementation and overriding
	 * {@link PageOfItems#read(int, int, User)}
	 * 
	 * @return An instance of a {@link PageOfItems} that will be used to read a
	 *         collection of items.
	 */
	protected PageOfItems<C, D, I> getPageInstance() {
		return new PageOfItems<C, D, I>();
	}

	/**
	 * Retrieves the ACL for this item. ACLs are uniquely identified by the ID's
	 * of the outer owning item. This method will also check the validity of
	 * referenced tokenId. Once the validity of the token has been established,
	 * the User object to which the token is assigned will be added to the
	 * context and retrievable via {@link #getContext()}
	 * 
	 * @param id
	 *            The id of the outer owning item.
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The ACL for this item.
	 * @throws AccessUnauthorizedException
	 *             If the tokenId is not provided, or if the tokenId does not
	 *             reference a valid AccessToken
	 */
	public ACLDto getAcl(final I id, final String tokenId)
			throws AccessUnauthorizedException {

		return authWrapAndExecute(tokenId, new ExceptionHandledLogic<ACLDto>() {

			@Override
			public ACLDto execute() {
				logger.debug(
						"Processing read request. itemType={}, id={}, accessToken={}",
						getManagedClass().getName(), id, tokenId);

				return doGetAclLogic(id);
			}

		});
	}

	/**
	 * Retrieves the ACL for this item. This method provides the hook through
	 * which extending classes can customize the behavior of getting an ACL. It
	 * differs from {@link #getAcl(Object, String)} in that it does not need to
	 * perform any additional auth checks and/or attempt to retrieve the
	 * AcessToken given the tokenId
	 * 
	 * @param id
	 *            The ID of the outer owning item
	 * @return The ACL associated with the outer owning item
	 */
	protected ACLDto doGetAclLogic(final I id) {
		@SuppressWarnings("rawtypes")
		ManagedCrud item = (ManagedCrud) getContext().getItemFactory()
				.findAndBuild(id, getManagedClass(), null);

		if (item == null)
			throw new CrudItemNotFoundException(id.toString(), ACL.class);

		item.getAcl().read();
		
		return item.getAcl().toDto();
	}

	/**
	 * Updates the ACL for this item. ACLs are uniquely identified by the ID's
	 * of the outer owning item. This method will also check the validity of
	 * referenced tokenId. Once the validity of the token has been established,
	 * the User object to which the token is assigned will be added to the
	 * context and retrievable via {@link #getContext()}
	 * 
	 * @param id
	 *            The ID of the outer owning item
	 * @param newState
	 *            The desired new state of the ACL
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The new state of the ACL persisted to the system
	 */
	public ACLDto updateAcl(final I id, final ACLDto newState,
			final String tokenId) {

		return authWrapAndExecute(tokenId, new ExceptionHandledLogic<ACLDto>() {

			@Override
			public ACLDto execute() {
				logger.debug(
						"Processing update request. itemType={}, id={}, accessToken={}",
						getManagedClass().getName(), id, tokenId);

				return doUpdateAclLogic(id, newState);
			}

		});
	}

	/**
	 * Updates the ACL for an item. This method provides the hook through which
	 * extending classes can customize the behavior of updating an ACL. It
	 * differs from {@link #updateAcl(Object, String)} in that it does not need
	 * to perform any additional auth checks and/or attempt to retrieve the
	 * AcessToken given the tokenId
	 * 
	 * @param id
	 *            The id of the ACL to update
	 * @param newState
	 *            The desired new state of the ACL
	 * @return The new state of the ACL persisted to the system
	 */
	protected ACLDto doUpdateAclLogic(final I id, final ACLDto newState) {
		if (newState == null)
			throw new BadRequestException(ManagedCrudService.UpdateItemNull);

		@SuppressWarnings("rawtypes")
		ManagedCrud existingItem = (ManagedCrud) getContext().getItemFactory()
				.findAndBuild(id, getManagedClass(), null);

		if (existingItem == null)
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());

		ACL newItem = (ACL) getContext().getItemFactory().build(newState, null);

		newItem = existingItem.updateACL(newItem);

		if (newItem != null)
			return newItem.toDto();
		else
			return null;
	}

	/**
	 * Retrieves a list of operations that the user is allowed to perform on the
	 * identified item. This method will also check the validity of referenced
	 * tokenId. Once the validity of the token has been established, the User
	 * object to which the token is assigned will be added to the context and
	 * retrievable via {@link #getContext()}
	 * 
	 * @param id
	 *            The id of the item whose user permitted operations to retrieve
	 * @param tokenId
	 *            A reference to a valid token identifying the User performing
	 *            the operation
	 * @return The list of operations the user is allowed to perform
	 */
	public OperationListDto getAllowedOps(final I id, final String tokenId) {

		return authWrapAndExecute(tokenId,
				new ExceptionHandledLogic<OperationListDto>() {

					@Override
					public OperationListDto execute() {
						logger.debug(
								"Processing read request. itemType={}, id={}, accessToken={}",
								getManagedClass(), id, tokenId);

						return doGetAllowedOpsLogic(id);
					}

				});
	}

	/**
	 * Retrieves a list of operations that the user is allowed to perform on the
	 * identified item. This method provides the hook through which extending
	 * classes can customize the behavior of getting allowed operations. It
	 * differs from {@link #getAllowedOps(Object, String)} in that it does not
	 * need to perform any additional auth checks and/or attempt to retrieve the
	 * AcessToken given the tokenId
	 * 
	 * @param id
	 *            The id of the item whose user permitted operations to retrieve
	 * @return The list of allowed operations
	 */
	protected OperationListDto doGetAllowedOpsLogic(final I id) {

		@SuppressWarnings("rawtypes")
		ManagedCrud item = (ManagedCrud) getContext().getItemFactory()
				.findAndBuild(id, getManagedClass(), null);

		if (item == null)
			throw new CrudItemNotFoundException(id.toString(),
					getManagedClass());
		
		item.doReadLogic();
		
		// Get the users permitted operations for this RulePlan's ACL
		@SuppressWarnings("unchecked")
        List<String> ops = item.operationsPermitted();

		OperationListDto dto = new OperationListDto();
		dto.setAllowedOperations(ops);

		return dto;
	}

	protected static final String AccessTokenNotFound = "AccessToken referenced by tokenId does not exist";
	protected static final String TokenIdNull = "tokenId cannot be null";

	/**
	 * Determines if the referenced token is valid. Note this is not the same as
	 * checking whether the user is allowed to perform the operation. That level
	 * of checking is left to the item being operated on.
	 * 
	 * This method is called as part of fulfilling a request. Its called just
	 * prior to the actual execution of said request.
	 * 
	 * @param tokenId
	 *            A reference to a token
	 * @return The AccessToken
	 * 
	 * @throws CrudIllegalArgException
	 *             The required arguments are null
	 * @throws AccessForbiddenException
	 *             If the AccessToken referenced by tokenId does not exist
	 */
	protected AccessToken validateToken(String tokenId)
			throws BadRequestException, AccessForbiddenException {
		if (tokenId == null)
			throw new AccessUnauthorizedException(TokenIdNull);

		AccessToken token = (AccessToken) getContext().getAccessFactory().findAndBuild(tokenId,
				AccessToken.class, null);

		if (token == null)
			throw new AccessUnauthorizedException(AccessTokenNotFound);

		token.touch();

		return token;
	}

	/**
	 * Wraps the provided {@link ExceptionHandledLogic} in authentication checks
	 * and exception handling. Once the user has been successfully
	 * authenticated, the User is provided to the current
	 * {@link ExecutionContext}, which is accessible via {@link #getContext()}
	 * 
	 * @param tokenId
	 *            A reference to a token
	 * @param l
	 *            the logic to wrap and execute
	 * @return The result of the execution
	 */
	protected <T> T authWrapAndExecute(String tokenId,
			ExceptionHandledLogic<T> l) {

		try {

			getContext().setAccessFactory(accessFactory);
			getContext().setTxnAccss(this.txnAccess);
			getContextProvider().getContext().setEm(this.getItemEntityManager());
			getContextProvider().getContext().setItemFactory(this.getItemFactory());
			getContextProvider().getContext().setLocator(this.getItemLocator());
			
			AccessToken token = validateToken(tokenId);
			
			getContext().setActor(token.getUser());

			return l.execute();

		} catch (CrudItemNotFoundException ex) {
			NotFoundException nfe = new NotFoundException(ex.getMessage(), ex);
			logger.info(AllExcpetionsMapper.exceptionAsJson(nfe));
			throw nfe;
		} catch (CrudItemValidationException ex) {
			ValidationException vex = null;
			        
			if(ex.getStatus() != null) {
    			switch(ex.getStatus()) {
        			case CONFLICT:
        			    vex = new ValidationExceptionConflict(ex.getMessage(), ex.getFieldErrors());
        			    break;
                    default:
                        vex = new ValidationException(ex.getMessage(), ex.getFieldErrors());
                        break;
    			}
			} else {
			    vex = new ValidationException(ex.getMessage(), ex.getFieldErrors());
			}
			
			logger.info(AllExcpetionsMapper.exceptionAsJson(vex));
			throw vex;
		} catch (AbstractSpecException ex) {
			logger.info(AllExcpetionsMapper.exceptionAsJson(ex));
			throw ex;
		} catch (Exception ex) {
			String message = "Something went wrong. Operation cannot complete.";
			this.logger.error(message, ex);
			throw new ServiceException(message, ex);
		} finally {
			getContextProvider().unset();
		}

	}
}
