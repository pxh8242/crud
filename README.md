This project contains a specification and a generic implementation of a RESTfull service pattern providing Create Read Update and Delete operations on a set of arbitrary resources.  The specification lives in the crud-spec project and a generic implementation lives in the crud-impl project.

# crud-spec #
crud-spec is built on top of jax-rs 1.1 and introduces a stateless RESTful service pattern that can be built into a CRUD enabled service that manages an arbitrary POJO.  Additionally, the specification formalizes the concept of authentication and authorization.  All service operations accept an AccessToken, which is associated to a user/role.

# crud-impl #
crud-impl is a generic implementaion of the spec. Without specificying the item being managed, the implementation generically handles the translation of the DTOs to their internal representation, manages the persistence of the item and manages the lifecycle of the AccessToken.  crud-impl introduces a dependency to JPA 2 and EJB 3.