package com.tcbakes.crud.tools;

public class CrudItemRegistrationException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CrudItemRegistrationException(){
		super();
	}
	
	public CrudItemRegistrationException(String msg){
		super(msg);
	}
	
	public CrudItemRegistrationException(String msg, Throwable t){
		super(msg, t);
	}
}
