package com.tcbakes.crud.tools;

public class NoBackingForDtoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoBackingForDtoException() {
		super();
	}
	public NoBackingForDtoException(String msg, Throwable cause){
		super(msg, cause);
	}

	public NoBackingForDtoException(String msg) {
		super(msg);
	}

}
