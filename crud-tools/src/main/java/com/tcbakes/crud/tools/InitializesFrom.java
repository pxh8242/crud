package com.tcbakes.crud.tools;

/**
 * Capable of initializing itself from D
 *
 * Deprecated. Use com.tcbakes.crud.init.InitializesFrom instead
 *
 * @param <D> The type from which an initialization is supported
 * @author Tristan Baker
 */
@Deprecated
public interface InitializesFrom<D> {

    /**
     * Initialize the state of this object from the state of the argument
     *
     * @param dto contains the state from which this object will be initialized
     */
    public void initializeFrom(D dto);


}
