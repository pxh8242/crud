package com.tcbakes.crud.tools.init;

import com.tcbakes.crud.tools.CrudItemsDirectory;

/**
 * Capable of initializing itself from D
 *
 * @param <D> The type from which an initialization is supported
 * @author Tristan Baker
 */
public interface InitializesFrom<D> {

    /**
     * Initialize the state of this object from the state of the provided argument
     *
     * @param dto contains the state from which this object will be initialized
     * @param dir an initialized crudItemsDirectory, which will allow implementors to
     *            propagate the initialization all the way through the object hierarchy
     */
    public void initializeFrom(D dto, CrudItemsDirectory dir);
}
