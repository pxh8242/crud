package com.tcbakes.crud.tools;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class as one that is associated with operations. Operations must be
 * delimited by a "_" (i.e. CREATE_USERGROUP, PUBLISH_RULEPLAN)
 * 
 * @since 1.9.4
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ManagedCrudOperation {

    /**
     * @return Valid operations
     */
    String[] value();
}
