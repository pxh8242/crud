package com.tcbakes.crud.tools;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;

import com.tcbakes.crud.registry.CrudItemRegistry;
import com.tcbakes.crud.spec.base.Dto;

/**
 * A directory that can be used to lookup backing classes for dto
 * objects. A backing class is defined as any object that implements the
 * {@link InitializableFrom} interface, is not abstract, and is annotated with
 * {@link ProvidesBackingForDto}
 * <p>
 * Call the {@link CrudItemsDirectory#initialize()} method to first build the
 * directory and then call {@link CrudItemsDirectory#getBackingInstance(Class)}
 * to retrieve an instance of a backing object given a dto
 * <p>
 * Note also that the base package name for any class marked with the
 * {@link ProvidesBackingForDto} annotation also needs to be provided in a
 * file with its name ending in crud-items.xml in /META-INF.
 * A good naming convention is to prefix the file name with the base package that is present in the file.
 * <p>
 * The contents of such a file might look something like this:
 * <p>
 * 
 * <pre>
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
 *   &lt;crud:CrudItemRegistry xmlns:crud=&quot;http://tcbakes.com/crud/registry&quot;
 *   xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
 *   xsi:schemaLocation=&quot;http://tcbakes.com/crud/registry Crud.xsd &quot;&gt;
 * 
 *   &lt;crud:BasePackges&gt;com.your.packge.here&lt;/crud:BasePackges&gt;
 * 
 * &lt;/crud:CrudItemRegistry&gt;
 * </pre>
 * 
 * @author Tristan Baker
 * @since 1.5
 */
public class CrudItemsDirectory {

    private static Logger logger = LoggerFactory
            .getLogger(CrudItemsDirectory.class);

    private HashMap<String, HashMap<Class<?>, Class<?>>> contextToDtoToCrudMap = new HashMap<>();

    private HashMap<Class<?>, HashMap<String, String>> operationsMap = new HashMap<>();
    
    // Contains list of all possible operations
    private HashSet<String> operationsSet = new HashSet<>();
    
    private ClassResolver classResolver;

    private ClassResolver getClassResolver() {
        if (classResolver == null)
            classResolver = new ClassResolver();

        return classResolver;
    }

    private PathMatchingResourcePatternResolver resourceResolver;

    private PathMatchingResourcePatternResolver getResourceResolver() {
        if (resourceResolver == null)
            resourceResolver = new PathMatchingResourcePatternResolver();

        return resourceResolver;
    }


    private ClassPathScanningCandidateComponentProvider scanner;

    public ClassPathScanningCandidateComponentProvider getScanner() {
        if(scanner == null) {
            scanner = new ClassPathScanningCandidateComponentProvider(true);
            scanner.addIncludeFilter(new AnnotationTypeFilter(
                    ProvidesBackingForDto.class));
        }
        return scanner;

    }



    /**
     * Initializes the CrudItemsDirectory. It will look for all configuration
     * files located on the classpath at "classpath*:META-INF/*crud-items.xml"
     * and scan for all {@link ProvidesBackingForDto} annotated classes in the
     * matching base packages listed in the crud-items.xml files
     * 
     * @throws CrudItemRegistrationException
     */
    public void initialize() throws CrudItemRegistrationException {

        try {
            JAXBContext context = JAXBContext
                    .newInstance(CrudItemRegistry.class);
            Unmarshaller u = context.createUnmarshaller();

            PathMatchingResourcePatternResolver resolver = getResourceResolver();
            Resource[] resources = resolver
                    .getResources("classpath*:META-INF/*crud-items.xml");

            logger.debug("Registering CRUD items");

            for (Resource r : resources) {
                logger.debug("Found crud-items.xml: {}", r.getURI());

                CrudItemRegistry registry = (CrudItemRegistry) u.unmarshal(r
                        .getInputStream());
                for (String basePackage : registry.getBasePackges()) {


                    for (BeanDefinition bd : getScanner()
                            .findCandidateComponents(basePackage)) {

                        Class<?> clazz = getClassResolver().resolveClassName(
                                bd.getBeanClassName(),
                                ClassUtils.getDefaultClassLoader());

                        ManagedCrudOperation managedCrudAnnotation = (ManagedCrudOperation) clazz
                                .getAnnotation(ManagedCrudOperation.class);

                        if (managedCrudAnnotation != null) {

                            for (String val : managedCrudAnnotation.value()) {
                                
                                if (val.contains("_"))
                                {
                                    String[] operation = val.split("_");
                                    
                                    if (!operationsMap.containsKey(clazz)) 
                                        operationsMap.put(clazz, new HashMap<String, String>());
    
                                    operationsMap.get(clazz).put(operation[0], val);
                                    operationsSet.add(val);
                                    logger.debug("Registering ManagedCrud operation: {0} ",
                                            val);
                                }
                            }
                        }
                        
                        if (Modifier.isAbstract(clazz.getModifiers())) {
                            throw new CrudItemRegistrationException(
                                    MessageFormat
                                            .format("Cannot register a dto backing class that is abstract. Offending class: {0}",
                                                    clazz.getName()));
                        }


                        
                        ProvidesBackingForDto annotation = (ProvidesBackingForDto) clazz
                                .getAnnotation(ProvidesBackingForDto.class);

                        if (annotation != null) {
                        	
                            boolean validConstructor = false;
                            for (Constructor<?> c : clazz.getConstructors()) {
                            	
                            	Class<?>[] parameterTypes = c.getParameterTypes();
                            	
                                if (parameterTypes.length == 0) {
                                    validConstructor = true;
                                    break;
                                }
                                
                                if (parameterTypes.length == 1) {
                                	if (parameterTypes[0] == annotation.value()) {
                                		validConstructor = true;
                                		break;
                                	}
                                }
                            }

                            if (!validConstructor)
                                throw new CrudItemRegistrationException(
                                        MessageFormat
                                                .format("Cannot register a dto backing class that does not provide a no-arg constructor or a constructor with a {1} type argument. Offending class: {0}",
                                                        clazz.getName(), annotation.value().getName()));
                        	
                            logger.debug("Registering CRUD item pair: [{}, {}] in context: '{}'",
                                    annotation.value().getName(),
                                    clazz.getName(),
                                    annotation.context());
                            if (!contextToDtoToCrudMap.containsKey(annotation
                                    .context()))
                                contextToDtoToCrudMap.put(annotation.context(),
                                        new HashMap<Class<?>, Class<?>>());

                            contextToDtoToCrudMap.get(annotation.context())
                                    .put(annotation.value(), clazz);
                        }
                    }
                }
            }

        } catch (JAXBException e) {
            throw new CrudItemRegistrationException(
                    "Could not parse the crud-items.xml file", e);

        } catch (IOException e) {
            throw new CrudItemRegistrationException(
                    "Problem reading the crud-items.xml file", e);
        }
    }

    /**
     * Finds and instantiates an appropriately registered backing class using
     * the default context
     * 
     * @param dtoClass The {@link Dto} class whose backing instance to instantiate
     * @return An instance of the backing class
     * @throws NoBackingForDtoException if no backing class can be found
     * @throws InstantiationException if the instantiation fails
     * @throws IllegalAccessException if the calls or its nullary constructor is not accessible
     */
    public InitializableFrom<? extends Dto<?>, ?> getBackingInstance(
            Class<?> dtoClass) throws NoBackingForDtoException,
            InstantiationException, IllegalAccessException {

        return getBackingInstance("default", dtoClass);

    }

    /**
     * Gets the backing class for a data transfer object in the given context
     * 
     * @param context a user defined context in which to find the backing instance
     * @param dtoClass the frotning class to find the backing class for
     * @return the backing class associated with the fronting class
     */
    public Class<?> getBackingClass(String context, Class<?> dtoClass) {
        if (contextToDtoToCrudMap.containsKey(context)) {

            HashMap<Class<?>, Class<?>> dtoToCrudMap = contextToDtoToCrudMap
                    .get(context);

            if (dtoToCrudMap.containsKey(dtoClass)) {
                return dtoToCrudMap.get(dtoClass);

            } else {
                throw new NoBackingForDtoException(MessageFormat.format(
                        "Could not find backing class for {0}.",
                        dtoClass.getName()));
            }
        } else {
            throw new NoBackingForDtoException(
                    MessageFormat
                            .format("Could not find backing class for {0}. Context ''{1}'' does not exist",
                                    dtoClass.getName(), context));
        }
    }
        
    /**
     * Retrieves the appropriately registered backing class using the provided context and instantiates an instance
     * 
     * @param context A user defined context in which to find the backing instance
     * @param dtoClass The {@link Dto} class whose backing instance to instantiate
     * @return An instance of the backing class
     * @throws NoBackingForDtoException if no backing class can be found
     * @throws InstantiationException if the instantiation fails
     * @throws IllegalAccessException if the calls or its nullary constructor is not accessible
     */
	@SuppressWarnings("unchecked")
	public InitializableFrom<? extends Dto<?>, ?> getBackingInstance(String context, Class<?> dtoClass)
			throws NoBackingForDtoException, InstantiationException, IllegalAccessException {

		Class<?> clazz = getBackingClass(context, dtoClass);

		if (!(com.tcbakes.crud.tools.init.InitializesFrom.class.isAssignableFrom(clazz)
				|| InitializableFrom.class.isAssignableFrom(clazz) || InitializesFrom.class.isAssignableFrom(clazz))) {
			throw new NoBackingForDtoException(MessageFormat.format(
					"Cannot initialize a dto backing class that does not implement {0} {1}, or {2} interface. Offending class: {3}",
					com.tcbakes.crud.tools.init.InitializesFrom.class.getName(), InitializableFrom.class.getName(),
					InitializesFrom.class.getName(), clazz.getName()));

		}
		return (InitializableFrom<? extends Dto<?>, ?>) clazz.newInstance();
	}

    /**
     * Returns an instance of the backing class using the default context
     *
     * @param dtoClass the fronting class
     * @return the backing class
     * @throws NoBackingForDtoException The backing class could not be found
     * @throws InstantiationException The backing class was found but couldn't be instantiated
     * @throws IllegalAccessException
     */
    public Object getBackingInstanceAsObject(Class<?> dtoClass) throws NoBackingForDtoException,
            InstantiationException, IllegalAccessException {
    
    	return getBackingInstanceAsObject("default", dtoClass);
    
    }

    /**
     * Returns an instance of the backing class using the specified context
     *
     * @param context the context
     * @param dtoClass the fronting class class
     * @return the backing class
     * @throws NoBackingForDtoException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public Object getBackingInstanceAsObject(
            String context, Class<?> dtoClass) throws NoBackingForDtoException,
            InstantiationException, IllegalAccessException {
    	
    	return getBackingClass(context, dtoClass).newInstance();

    }


    /**
     * Finds an instance of a backing class in the default context, initializes it and then returns it.
     * <p/>
     * This method will only work for backing classes that implement {@link
     * com.tcbakes.crud.tools.init.InitializesFrom}
     *
     * @param dto the fronting class
     * @return an initialized instance of the backing class.  If the dto parameter is null, null will be returned.
     * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
     */
    public Object getBackingInstanceAndInitialize(Object dto) throws NoBackingForDtoException{
        if(dto == null)
            return null;

        return getBackingInstanceAndInitialize("default", dto);
    }

    /**
     * Finds an instance of a backing class in the provided context, initializes it and then returns it.
     * <p/>
     * This method will only work for backing classes that implement {@link
     * com.tcbakes.crud.tools.init.InitializesFrom}
     *
     * @param context the context
     * @param dto the fronting class
     * @return an initialized instance of the backing class.  If the dto parameter is null, null will be returned.
     * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
     */
    public Object getBackingInstanceAndInitialize(String context, Object dto) throws NoBackingForDtoException {

        if(dto == null)
            return null;

        try {
            Object o = getBackingInstanceAsObject(context, dto.getClass());
            com.tcbakes.crud.tools.init.InitializesFrom i = (com.tcbakes.crud.tools.init.InitializesFrom)o;
            i.initializeFrom(dto, this);
            return o;
        } catch (Exception e) {
            throw new NoBackingForDtoException("In order for the initialization to be propogated, all backing instances " +
                    "must implement the com.tcbakes.crud.tools.init.InitializesFrom interface, providing an implementation of the " +
                    "'void initializeFrom(D,CrudItemsDirectory)' method", e);
        }
    }

    /**
	 * Finds the backing class of the provided object in the default context and uses a constructor to return a new 
	 * instance of that backing class.
	 * <p/>
	 * This method will try to use one of two constructors. It will first check if there is a constructor that uses the
	 * fronting class as a parameter and then attempt to use the nullary constructor.
	 *
	 * @param dto the fronting class
	 * @return an initialized instance of the backing class. If the dto parameter is null, null will be returned.
	 * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
     */
    public Object getBackingInstanceAndConstruct(Object dto) throws NoBackingForDtoException {
    	return getBackingInstanceAndConstruct("default", dto);
    }
    
	/**
	 * Finds the backing class of the provided object in the provided context and uses a constructor to return a new 
	 * instance of that backing class.
	 * <p/>
	 * This method will try to use one of two constructors. It will first check if there is a constructor that uses the
	 * fronting class as a parameter and then attempt to use the nullary constructor.
	 *
	 * @param context the context
	 * @param dto the fronting class
	 * @return an initialized instance of the backing class. If the dto parameter is null, null will be returned.
	 * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
	 * @throws NoSuchMethodException 
	 */
	public Object getBackingInstanceAndConstruct(String context, Object dto) throws NoBackingForDtoException {

		if (dto == null)
			return null;

		Class<?> dtoClass = dto.getClass();
		Class<?> backingClass = getBackingClass(context, dtoClass);
		Constructor<?> constructor = null;

		try {

			// Attempt to call parameterized constructor
			try {
				constructor = backingClass.getDeclaredConstructor(dtoClass);
				return constructor.newInstance(dto);

			// Try non-parameterized constructor
			} catch (NoSuchMethodException e) {
				constructor = backingClass.getDeclaredConstructor();
				return constructor.newInstance();
			}

		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException e) {
			throw new NoBackingForDtoException(MessageFormat.format(
					"No valid constructor found for class: {0}. Must implement a nullary constructor or a constructor with a {1} type argument",
					backingClass.getName(), dtoClass.getName()));
		}
	}

    /**
     * Iterates over every object in the Collection, finds an instance of a backing class in the provided context, initializes it and then returns in a new Collection.
     * <p/>
     * This method will only work for backing classes that implement {@link
     * com.tcbakes.crud.tools.init.InitializesFrom}
     *
     * @param context The context
     * @param dtos The collection of fronting objects
     * @return an initialized instance of the backing class.  If the dto parameter is null, null will be returned.
     * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
     */
    public Collection getBackingInstanceAndInitializeCollection(String context, Collection dtos) throws NoBackingForDtoException {

        if(dtos == null)
            return null;

        ArrayList<Object> a = new ArrayList<>();

        for(Object dto : dtos){
            a.add(getBackingInstanceAndInitialize(context, dto));
        }

        return a;
    }

    /**
     * Iterates over every object in the Collection, finds an instance of a backing class in the default context, initializes it and then returns it in a new Collection.
     * <p/>
     * This method will only work for backing classes that implement {@link
     * com.tcbakes.crud.tools.init.InitializesFrom}
     *
     * @param dtos The collection of fronting objects
     * @return an initialized instance of the backing class.  If the dtos parameter is null, null will be returned
     * @throws NoBackingForDtoException If there was a problem trying to find, instantiate or initialize it.
     */
    public Collection getBackingInstanceAndInitializeCollection(Collection dtos) throws NoBackingForDtoException {

       return getBackingInstanceAndInitializeCollection("default", dtos);
    }
    
    /**
     * Retrieves a list of all valid operations by class name
     */
    public HashMap<String, String> getOperationsByClass(Class<?> key) {
        return operationsMap.get(key);
    }
    
    /**
     * Retrieves a list of all valid operations 
     */
    public HashSet<String> getValidOperations() {
        return operationsSet;
    }
}
