package com.tcbakes.crud.tools.testclasses;

import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.ProvidesBackingForDto;
import com.tcbakes.crud.tools.init.InitializesFrom;

/**
 * Created by tbaker on 9/4/15.
 */
@ProvidesBackingForDto(Baz.class)
public class _Baz implements InitializesFrom<Baz> {
    String alpha;
    public String getAlpha() {
        return alpha;
    }

    @Override
    public void initializeFrom(Baz dto, CrudItemsDirectory dir) {
        alpha = dto.getAlpha();
    }
}
