package com.tcbakes.crud.tools.testclasses;

/**
 * Created by tbaker on 9/4/15.
 */
public class Bar {
    String beta;

    public String getBeta() {
        return beta;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }
}
