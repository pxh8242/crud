package com.tcbakes.crud.tools2.constructedclasses;

import com.tcbakes.crud.tools.ProvidesBackingForDto;

@ProvidesBackingForDto(ConstructedBar.class)
public class _ConstructedBar {
	
	Long _id;
	
	public _ConstructedBar() {
		_id = 12345L;
	}
	
	public Long getId() {
		return _id;
	}

	public void setId(Long _id) {
		this._id = _id;
	}
}
