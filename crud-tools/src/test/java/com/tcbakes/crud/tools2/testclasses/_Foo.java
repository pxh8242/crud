package com.tcbakes.crud.tools2.testclasses;

import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.ProvidesBackingForDto;
import com.tcbakes.crud.tools.init.InitializesFrom;

import java.util.List;

/**
 * Created by tbaker on 9/4/15.
 */
@ProvidesBackingForDto(Foo.class)
public class _Foo implements InitializesFrom<Foo> {

    _Bar bar;

    List<_Baz> bazes;

    _Bat bat;

    public List<_Baz> getBazes() {
        return bazes;
    }

    public void setBazes(List<_Baz> bazes) {
        this.bazes = bazes;
    }

    public _Bar getBar() {
        return bar;
    }

    public void setBar(_Bar bar) {
        this.bar = bar;
    }

    public _Bat getBat() {
        return bat;
    }

    public void setBat(_Bat bat) {
        this.bat = bat;
    }

    @Override
    public void initializeFrom(Foo dto, CrudItemsDirectory dir) {
        bar = (_Bar) dir.getBackingInstanceAndInitialize(dto.getBar());
        bazes = (List<_Baz>) dir.getBackingInstanceAndInitializeCollection(dto.getBazes());
        bat = (_Bat) dir.getBackingInstanceAndInitialize(dto.getBat());
    }
}
