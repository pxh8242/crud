package com.tcbakes.crud.spec.access;


public class FakeUser extends UserDto {

	private static final long serialVersionUID = 1L;

	String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
