package com.tcbakes.crud.spec.access;

import java.util.List;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import com.tcbakes.crud.spec.base.ManagedDto;

/**
 * A Role identifies the set of operations a user is permitted to perform
 * 
 * @author Tristan Baker
 * 
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class UserGroupDto extends ManagedDto<Long> {

	private static final long serialVersionUID = 1L;

	private String name;
    private List<String> operations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getOperations() {
        return operations;
    }

    public void setOperations(List<String> operations) {
        this.operations = operations;
    }
}
