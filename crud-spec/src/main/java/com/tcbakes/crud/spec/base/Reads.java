package com.tcbakes.crud.spec.base;

import com.tcbakes.crud.spec.access.AuthenticateServiceREST;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;

/**
 * Defines a generic contract for retrieving the state of a system item
 *
 * @param <D> The {@link Dto} being managed 
 * @param <I> The type of the {@link Dto}'s primary id.
 *  
 * @author Tristan Baker
 */
public interface Reads<D extends Dto<I>, I> {

	/**
	 * Retrieve an item by its primary id.  Access is granted/denied via
	 * the tokenId, which identifies the actor performing the operation.
	 * 
	 * @param id
	 *            The id of the item to retrieve
	 * @param tokenId
	 *            The reference to the token granting access to perform this
	 *            operation
	 * @return The item if found.
	 * 
	 * @throws BadRequestException
	 *             If the id is null
	 * @throws NotFoundException
	 *             If the item reference by the id is not found
	 * @throws AccessForbiddenException
	 *             If the AccessToken referenced by tokenId does not grant
	 *             access to this operation
	 * @throws ServiceException
	 *             Any other error
	 * @see AuthenticateServiceREST#authenticate(com.tcbakes.crud.spec.access.CredentialDto)
	 */
	public D read(I id, String tokenId)
			throws BadRequestException, NotFoundException,
			AccessForbiddenException, ServiceException;
}
