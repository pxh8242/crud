package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.Response.Status;

/**
 * An internal error occurred and the request could not be completed. This
 * is equivalent to a HTTP 500 response code
 * 
 * @see javax.ws.rs.core.Response.Status#INTERNAL_SERVER_ERROR
 * @author Tristan Baker
 * 
 */
public class ServiceException extends AbstractSpecException {

    private static final long serialVersionUID = 1L;

    public ServiceException() {
    }

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.INTERNAL_SERVER_ERROR;
    }

}
