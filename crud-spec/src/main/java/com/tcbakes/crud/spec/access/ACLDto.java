package com.tcbakes.crud.spec.access;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.tcbakes.crud.spec.base.Dto;

@JsonIgnoreProperties("id")
public class ACLDto extends Dto<Long> {

    private static final long serialVersionUID = 1L;

    private Long owningUserId;
    private List<String> userGroups;
    
    public Long getOwningUserId() {
        return owningUserId;
    }
    
    public void setOwningUserId(Long userId) {
        this.owningUserId = userId;
    }
    
    public List<String> getUserGroups() {
        return userGroups;
    }
    
    public void setUserGroups(List<String> groups) {
        this.userGroups = groups;
    }
}
