package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.Response.Status;

/**
 * The requested item was not found.  This is
 * equivalent to a HTTP 404 response code
 * 
 * @see javax.ws.rs.core.Response.Status#NOT_FOUND
 * @author Tristan Baker
 *
 */
public class NotFoundException extends AbstractSpecException{

	private static final long serialVersionUID = 1L;

	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String msg) {
		super(msg);
	}
	
	public NotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

	@Override
	public Status getHttpStatusCode() {
		return Status.NOT_FOUND;
	}
}
