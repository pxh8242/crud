package com.tcbakes.crud.spec.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationDetails extends SimpleExceptionDetails {
    
    private Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();

    public ValidationDetails(){
    	
    }
    
    public ValidationDetails(Map<String, List<String>> failures) {
        super("Failed Validation Check");
        
        this.fieldErrors = failures;
    }
    
    public Map<String, List<String>> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, List<String>> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

}
