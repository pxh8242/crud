package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.Response.Status;

/**
 * The request can not be completed due to insufficient privileges. This is
 * equivalent to a HTTP 403 response code
 * 
 * @see javax.ws.rs.core.Response.Status#FORBIDDEN
 * @author Tristan Baker
 */
public class AccessForbiddenException extends AbstractSpecException {

    private static final long serialVersionUID = 1L;

    public AccessForbiddenException() {
    }

    public AccessForbiddenException(String msg) {
        super(msg);
    }

    public AccessForbiddenException(String msg, Throwable cause) {
        super(msg, cause);
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.FORBIDDEN;
    }
}
