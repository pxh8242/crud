package com.tcbakes.crud.spec.access;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import com.tcbakes.crud.spec.base.ManagedDto;

/**
 * An AccessToken is issued to a User upon validation of their credentials.
 * 
 * @author Tristan Baker
 */
public class AccessTokenDto extends ManagedDto<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	String id;
	Long userId;
	List<String> groups;
	Calendar mostRecentlyActive;
	Boolean enabled;

	/**
	 * @return The unique ID of this access token. This ID will need to be
	 *         included in all future requests to authenticated services
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return The ID of the user to whom this AccessToken is granted
	 */
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return A list of one or more roles assigned to the user
	 */
	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> grantedAuthorities) {
		this.groups = grantedAuthorities;
	}

	/**
	 * @return The time that this AccessToken was last used to perform some
	 *         operation
	 */
	public Calendar getMostRecentlyActive() {
		return mostRecentlyActive;
	}

	public void setMostRecentlyActive(Calendar mostRecentlyActive) {
		this.mostRecentlyActive = mostRecentlyActive;
	}

	/**
	 * @return A disabled AccessToken will never allow access to authenticated
	 *         services
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

}
